TEST('explode', function () {
    let {day, month, year, hours, minutes, seconds} = new Date('2018-03-31T14:15:33').explode();

    OK(day === 31);
    OK(month === 3);
    OK(year === 2018);
    OK(hours === 14);
    OK(minutes === 15);
    OK(seconds === 33);
});

TEST('explode utc', function () {
    let {day, month, year, hours, minutes, seconds} = new Date('2018-03-31T14:15:33').explode(true);

    OK(day === 31);
    OK(month === 3);
    OK(year === 2018);
    OK(hours === 12);
    OK(minutes === 15);
    OK(seconds === 33);
});

TEST('startOf', function () {
    _ok = (d1, d2) => OK(d1.format('yyyy-MM-dd') === d2.format('yyyy-MM-dd'));
    _fail = (d1, d2) => FAIL(d1.format('yyyy-MM-dd') === d2.format('yyyy-MM-dd'));

    _fail(new Date('2018-03-31'), new Date('2018-04-10').startOf('month'));
    _ok(new Date('2018-04-01'), new Date('2018-04-10').startOf('month'));
    _fail(new Date('2018-04-02'), new Date('2018-04-10').startOf('month'));

    _fail(new Date('2018-12-31'), new Date('2018-04-10').startOf('year'));
    _ok(new Date('2018-01-01'), new Date('2018-04-10').startOf('year'));
    _fail(new Date('2018-01-02'), new Date('2018-04-10').startOf('year'));
});

TEST('endOf', function () {
    _ok = (d1, d2) => OK(d1.format('yyyy-MM-dd') === d2.format('yyyy-MM-dd'));
    _fail = (d1, d2) => FAIL(d1.format('yyyy-MM-dd') === d2.format('yyyy-MM-dd'));

    _fail(new Date('2018-04-29'), new Date('2018-04-10').endOf('month'));
    _ok(new Date('2018-04-30'), new Date('2018-04-10').endOf('month'));
    _ok(new Date('2018-02-28'), new Date('2018-02-10').endOf('month'));
    _ok(new Date('2020-02-29'), new Date('2020-02-10').endOf('month'));
    _fail(new Date('2018-05-01'), new Date('2018-04-10').endOf('month'));

    _fail(new Date('2018-12-30'), new Date('2018-04-10').endOf('year'));
    _ok(new Date('2018-12-31'), new Date('2018-04-10').endOf('year'));
    _fail(new Date('2019-01-01'), new Date('2018-04-10').endOf('year'));
});

TEST('set', function () {
    OK(new Date('2018-02-03T12:34:56').set('s', 1).format('s') === '1');
    OK(new Date('2018-02-03T12:34:56').set('m', 1).format('m') === '1');
    OK(new Date('2018-02-03T12:34:56').set('h', 1).format('h') === '1');
    OK(new Date('2018-02-03T12:34:56').set('M', 1).format('M') === '1');
    OK(new Date('2018-02-03T12:34:56').set('d', 1).format('d') === '1');
    OK(new Date('2018-02-03T12:34:56').set('y', 2000).format('yyyy') === '2000');
});
