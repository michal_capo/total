TEST('flatten', function () {
    OK([[1, 2], [3]].flatten().join('') === '123');
});
