TEST('repeat', function () {
    OK((3).repeat('a') === 'aaa');
});

TEST('vat', function () {
    OK((120).vat(20) === 20);
    OK((100).vat(20) === 16.67);
    OK((100).vat(20, 1) === 16.7);
    OK((100).vat(20, 3) === 16.667);
});

TEST('vat_rest', function () {
    OK((120).vat_rest(20) === 100);
    OK((100).vat_rest(20) === 83.33);
    OK((100).vat_rest(20, 1) === 83.3);
    OK((100).vat_rest(20, 3) === 83.333);
});

TEST('plus', function () {
    FAIL(3.1 + 3.2 === 6.3);
    OK((3.1).plus(3.2) === 6.3);
});

TEST('minus', function () {
    FAIL(3.1 - 3.2 === -0.1);
    OK((3.1).minus(3.2) === -0.1);
});

TEST('multiply', function () {
    FAIL(3.1 * 3.2 === 9.92);
    OK((3.1).multiply(3.2) === 9.92);
});

TEST('modulo', function () {
    FAIL(3.2 % 3.1 === 0.1);
    OK((3.2).modulo(3.1) === 0.1);
});

TEST('round_to', function () {
    OK((123.34).round_to(0.05) === 123.35);
    OK((123.34).round_to(0.5) === 123.5);
    OK((123.34).round_to() === 123);
    OK((123.34).round_to(1) === 123);
    OK((123.34).round_to(5) === 125);
    OK((123.34).round_to(10) === 120);
    OK((127).round_to(20) === 120);
    OK((137).round_to(20) === 140);
    OK((123.34).round_to(100) === 100);
    OK((123.34).round_to(100) === 100);
    OK((30).round_to(16) === 32);
    OK((30).round_to(17) === 34);
});
