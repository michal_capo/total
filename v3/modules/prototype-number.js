exports.id = 'prototype-number';
exports.version = '0.0.1';
exports.dependencies = ['utils'];
exports.install = function () {
    U.check(Number.prototype.round_to, 'undefined');
    Number.prototype.round_to = function (precision = 1) {
        return Number((Math.round(this / precision) * precision).toPrecision(15));
    };

    U.check(Number.prototype.repeat, 'undefined');
    Number.prototype.repeat = function (val) {
        let result = '';

        for (let i = 0; i < this; i++) {
            result += val;
        }

        return result;
    };

    U.check(Number.prototype.vat, 'undefined');
    Number.prototype.vat = function (vat, precision = 2) {
        return (this / (100 + vat) * vat).round(precision);
    };

    U.check(Number.prototype.vat_rest, 'undefined');
    Number.prototype.vat_rest = function (vat, precision = 2) {
        return (this / (100 + vat) * 100).round(precision);
    };

    U.check(Number.prototype.plus, 'undefined');
    Number.prototype.plus = function (val) {
        return ((this + val).floor(14));
    };

    U.check(Number.prototype.minus, 'undefined');
    Number.prototype.minus = function (val) {
        return Number((this - val).toPrecision(14));
    };

    U.check(Number.prototype.multiply, 'undefined');
    Number.prototype.multiply = function (val) {
        return ((this * val).floor(14));
    };

    U.check(Number.prototype.divide, 'undefined');
    Number.prototype.divide = function (val) {
        return (this / val);
    };

    U.check(Number.prototype.modulo, 'undefined');
    Number.prototype.modulo = function (val) {
        return ((this % val).floor(14));
    };
};
