exports.id = 'mongo';
exports.install = function () {
    F.mongo = {
        connect: function (connection_string) {
            const {MongoClient} = require('mongodb');

            return new Promise(function (resolve, reject) {
                MongoClient.connect(connection_string, async function (err, connection) {
                    if (err)
                        reject(err);
                    else
                        connection.collections()
                            .then(collections => {
                                const temp = {
                                    connection,
                                    db: (name) => connection.db(name),
                                    collection: (name) => connection.collection(name)
                                };

                                collections.forEach(function (c) {
                                    temp[c.s.name] = c;
                                });

                                resolve(temp);
                            })
                            .catch(e => reject(e))
                        ;
                });

            });
        }
    };
};
exports.uninstall = function () {
    delete F.mongo;
};