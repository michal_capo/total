exports.id = 'mssql';
exports.version = '0.0.2';
exports.dependencies = ['utils'];
exports.install = function () {
    MSSQL = {};
    MSSQL.table_name = function (db, table, schema = 'dbo') {
        U.check(db, String);
        U.check(table, String);

        return '{0}.{1}.{2}'.format(db, schema, table);
    };

    MSSQL.get_value = function (result, field = '') {
        return new Promise((resolve, reject) => {
            result
                .then((result) => resolve(result.length > 0 && result[0] && result[0][field]))
                .catch((e) => reject(e))
        });
    };

    MSSQL.get_row = function (result) {
        return new Promise((resolve, reject) => {
            result
                .then((result) => resolve(result.length > 0 && result[0]))
                .catch((e) => reject(e))
        });
    };

    MSSQL.get_result = function (result) {
        return new Promise((resolve, reject) => {
            result
                .then((result) => resolve(result))
                .catch((e) => reject(e))
        });
    };

    MSSQL.raw_value = function (connection, sql, field = '') {
        U.check(sql, String);

        return MSSQL.get_value(connection.raw(sql), field);
    };

    MSSQL.raw_row = function (connection, sql) {
        U.check(sql, String);

        return MSSQL.get_row(connection.raw(sql));
    };

    MSSQL.raw = function (connection, sql) {
        U.check(sql, String);

        return MSSQL.get_result(connection.raw(sql));
    };

    MSSQL.has_db = async function (connection, db) {
        U.check(db, String);

        return (await MSSQL.get_value(connection.raw("select count(*) from master.sys.sysdatabases WHERE name = '{0}'".format(db)))) > 0
    };

    MSSQL.check_db = async function (connection, db) {
        if (!await MSSQL.has_db(connection, db)) {
            throw new Error('Database "{0}" doesn`t exists.'.format(db));
        }
    };

    MSSQL.has_table = async function (connection, db, table, schema = 'dbo') {
        U.check(db, String);
        U.check(table, String);
        U.check(schema, String);

        return (await MSSQL.raw_value(connection, "select count(*) from {0}.INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = '{1}' AND TABLE_SCHEMA = '{2}'".format(db, table, schema))) > 0
    };

    MSSQL.check_table = async function (connection, db, table, schema = 'dbo') {
        if (!await MSSQL.has_table(connection, db, table, schema)) {
            throw new Error('Table "{0}.{1}.{2}" doesn`t exists.'.format(db, schema, table));
        }
    };

    MSSQL.has_column = async function (connection, db, table, column, schema = 'dbo') {
        U.check(db, String);
        U.check(table, String);
        U.check(column, String);
        U.check(schema, String);

        return (await MSSQL.raw_value(connection, "select count(*) from {0}.INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = '{1}' AND COLUMN_NAME = '{2}' AND TABLE_SCHEMA='{3}'".format(db, table, column, schema))) > 0;
    };

    MSSQL.check_column = async function (connection, db, table, column, schema = 'dbo') {
        if (!await MSSQL.has_column(connection, db, table, column, schema)) {
            throw new Error('Column "{3}" in "{0}.{1}.{2}" doesn`t exists.'.format(db, schema, table, column));
        }
    };

    MSSQL.has_permission_db = async function (connection, db) {
        U.check(db, String);

        const sql = `SELECT HAS_PERMS_BY_NAME('{0}', 'DATABASE', 'CONNECT') AS permission`.format(db);
        return (await MSSQL.raw_value(connection, sql, 'permission')) === 1;
    };

    MSSQL.check_permission_db = async function (connnection, db) {
        if (!await MSSQL.has_permission_db(connnection, db)) {
            throw new Error(`You dont have permission to access '${db}' database.`);
        }
    };

    MSSQL.has_permission_table = async function (connection, db, table, perms, schema = 'dbo') {
        U.check(db, String);
        U.check(table, String);
        U.check(schema, String);
        U.check(perms, Array);

        if (perms.indexOf('SELECT') === -1) {
            perms.push(['SELECT']);
        }

        let sql = `SELECT `.format(db);

        perms.forEach(function (perm, index) {
            sql += `HAS_PERMS_BY_NAME('{0}', 'OBJECT', '{1}')`.format(MSSQL.table_name(db, table, schema), perm);

            if (index !== perms.length - 1) {
                sql += ' & '
            }
        });

        sql += ' AS permission;';

        return (await MSSQL.raw_value(connection, sql, 'permission')) === 1;
    };

    MSSQL.check_permission_table = async function (connnection, db, table, perms, schema = 'dbo') {
        if (!await MSSQL.has_permission_table(connnection, db, table, perms, schema)) {
            throw new Error(`You dont have permissions '${perms}' for '${table}' table in '${db}' database.`);
        }
    };

    MSSQL.database = function (connection, db = 'StwPh_sys') {
        U.check(connection, Object);
        U.check(db, String);

        return {
            db_name() {
                return db;
            },

            table_name(table, schema) {
                return MSSQL.table_name(db, table, schema);
            },

            has_table(table, schema) {
                return MSSQL.has_table(connection, db, table, schema);
            },

            has_column(table, column, schema) {
                return MSSQL.has_column(connection, db, table, column, schema);
            },

            check_table(table, schema) {
                MSSQL.check_table(connection, db, table, schema);
            },

            check_column(table, column, schema) {
                MSSQL.check_column(connection, db, table, column, schema);
            },

            raw_value(sql, field = '') {
                return MSSQL.raw_value(connection, sql, field);
            },

            raw_row(sql) {
                return MSSQL.raw_row(connection, sql);
            },

            raw_result(sql) {
                return MSSQL.raw(connection, sql);
            },

            has_permission_db() {
                return MSSQL.has_permission_db(connection, db);
            },

            has_permission_table(table, perms, schema) {
                return MSSQL.has_permission_table(connection, db, table, perms, schema);
            },

            check_permission_db() {
                return MSSQL.check_permission_db(connection, db);
            },

            check_permission_table(table, perms, schema) {
                return MSSQL.check_permission_table(connection, db, table, perms, schema);
            },

            table(table, schema) {
                return connection(this.table_name(table, schema))
            }
        };
    };
};
exports.uninstall = function () {
    delete MSSQL;
};
