exports.id = 'prototype-date';
exports.version = '0.0.1';
exports.dependencies = ['utils'];
exports.install = function () {
    U.check(Date.prototype.explode, 'undefined');
    Date.prototype.explode = function (to_utc = false) {
        let temp = {
            day: this.getDate(),
            day_week: this.getDay(),
            month: this.getMonth() + 1,
            year: this.getFullYear(),
            hours: this.getHours(),
            minutes: this.getMinutes(),
            seconds: this.getSeconds(),
            milis: this.getMilliseconds(),
        };

        if (to_utc) {
            temp = {
                day: this.getUTCDate(),
                day_week: this.getUTCDay(),
                month: this.getUTCMonth() + 1,
                year: this.getUTCFullYear(),
                hours: this.getUTCHours(),
                minutes: this.getUTCMinutes(),
                seconds: this.getUTCSeconds(),
                milis: this.getUTCMilliseconds()
            }
        }

        return temp;
    };


    /**
     * Set date to start of month or year.
     *
     * @param type, can be month or year
     */
    U.check(Date.prototype.startOf, 'undefined');
    Date.prototype.startOf = function (type = 'month') {
        const {day, month} = this.explode();

        let dt = new Date(this.getTime());

        switch (type) {
            case 'month':
                dt = dt.add('d', -1 * (day - 1));
                break;
            case 'year':
                dt = dt.add('d', -1 * (day - 1)).add('M', -1 * (month - 1));
                break;
        }

        return dt;
    };

    /**
     * Set date to end of month or year.
     *
     * @param type, can be month or year
     */
    U.check(Date.prototype.endOf, 'undefined');
    Date.prototype.endOf = function (type = 'month') {
        const {day, month, year} = this.explode();
        const leap = (year % 100 === 0) ? (year % 400 === 0) : (year % 4 === 0);
        const days_count = [31, 28 + (leap ? 1 : 0), 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];

        let dt = new Date(this.getTime());

        switch (type) {
            case 'month':
                dt = dt.add('d', days_count[month - 1] - day);
                break;
            case 'year':
                dt = dt.add('M', 12 - month).add('d', 31 - day);
                break;
        }

        return dt;
    };

    U.check(Date.prototype.set, 'undefined');
    Date.prototype.set = function (type, count) {
        let dt = new Date(this.getTime());

        switch (type) {
            case 's':
            case 'ss':
            case 'sec':
            case 'second':
            case 'seconds':
                dt = new Date(dt.setSeconds(count));
                break;
            case 'm':
            case 'mm':
            case 'min':
            case 'minute':
            case 'minutes':
                dt = new Date(dt.setMinutes(count));
                break;
            case 'h':
            case 'hh':
            case 'H':
            case 'HH':
            case 'hour':
            case 'hours':
                dt = new Date(dt.setHours(count));
                break;
            case 'M':
            case 'MM':
            case 'MMM':
            case 'MMMM':
            case 'month':
                dt = new Date(dt.setMonth(count - 1));
                break;
            case 'd':
            case 'dd':
            case 'ddd':
            case 'dddd':
            case 'day':
                dt = new Date(dt.setDate(count));
                break;
            case 'y':
            case 'yy':
            case 'yyy':
            case 'yyyy':
            case 'year':
                dt = new Date(dt.setFullYear(count));
                break;
        }

        return dt;
    }
};
