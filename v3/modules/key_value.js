exports.id = 'key_value';
exports.version = '0.0.1';
exports.install = function () {
    const levelup = require('levelup');
    const leveldown = require('leveldown');
    const encode = require('encoding-down');

    KEY_VALUE = function (path) {
        return levelup(encode(leveldown(path), {valueEncoding: 'json'}));
    };
};
exports.uninstall = function () {
    delete KEY_VALUE;
};

