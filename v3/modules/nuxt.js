exports.id = 'nuxt';
exports.version = '0.0.2';
exports.install = function () {
    const config_path = F.config['nuxt-config-path'] || './nuxt.config.js';
    const config_default = {
        build: {
            // vendor: ['axios']
        },
        srcDir: F.config['nuxt-src-dir'] || 'nuxt/'
    };

    F.path.exists(config_path, async function (exists) {
        try {
            const options = exists ? require(F.path.root(config_path)) : config_default;
            const {Nuxt, Builder} = require('nuxt');
            const nuxt = new Nuxt(options);
            const builder = new Builder(nuxt);

            await builder.build();
            F.middleware('nuxt', nuxt.render);
        } catch (e) {
            F.log(e);
        }
    });

    /**
     * Test url, if not start with '/api' then use nuxt middleware to handle the request.
     */
    const reg = /^(?!\/api)[/A-Za-z0-9_.]*$/;

    /**
     * this should not be execute
     */
    function action() {
        throw new Error('nuxt middleware not working');
    }

    F.route(url => reg.test(url), action, ['#nuxt']);
    F.file(url => reg.test(url.uri.path), action, ['#nuxt']);
};
