exports.id = 'http';
exports.version = '0.0.1';
exports.dependencies = ['utils'];
exports.install = function () {
    const Http = require('http');
    const Url = require('url');

    let http_on_stream = function (ok, fail, encoding = 'utf8') {
        return function (res) {
            let temp = {body: ''};

            temp.headers = res.headers;
            temp.httpVersion = res.httpVersion;
            temp.method = res.method;
            temp.statusCode = res.statusCode;
            temp.statusMessage = res.statusMessage;
            temp.url = res.url;

            res.setEncoding(encoding);

            res.on("data", data => temp.body += data);
            res.on("end", () => ok(temp));
            res.on('error', e => fail(e))
        }
    };

    let create_http_part = function (method) {
        U.check(method, String);

        return function (url, body, encoding = 'utf8') {
            return new Promise(function (resolve, reject) {
                let ok = response => response.statusCode >= 400 ? reject(response) : resolve(response);
                let fail = response => reject(response);

                let _stream = http_on_stream(ok, fail, encoding);
                let _url = Url.parse(url, true);

                _url.method = method;

                let _req = Http.request(_url, _stream);

                if (body) {
                    _req.write(Buffer.from(JSON.stringify(body)));
                }
                _req.end();
            });
        }
    };

    HTTP = function HTTP(url, body, encoding = 'utf8') {
        let parsed = url.split(/(\w+\s)?(.*)/);

        switch (U.uppercase(parsed[1] == null ? null : parsed[1].trim())) {
            case 'POST':
                return HTTP.post(parsed[2], body, encoding);

            case 'PUT':
                return HTTP.put(parsed[2], body, encoding);

            case 'DELETE':
                return HTTP.delete(parsed[2], body, encoding);

            case 'HEAD':
                return HTTP.head(parsed[2], body, encoding);

            default:
                return HTTP.get(parsed[2], body, encoding)
        }
    };

    HTTP.get = create_http_part('GET');
    HTTP.post = create_http_part('POST');
    HTTP.put = create_http_part('PUT');
    HTTP.delete = create_http_part('DELETE');
    HTTP.head = create_http_part('HEAD');
};

exports.uninstall = function () {
    delete HTTP;
    http_on_stream = undefined;
    create_http_part = undefined;
};
