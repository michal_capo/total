exports.dependencies = ['v4_utils'];
exports.install = function (options) {

    U.check(options, Object);
    U.check(options.secret, String);
    U.check(options.salt, String);

    // This object will contain all online users (their sessions)
    const ONLINE = {};

    // Cookie name
    const COOKIE = options.cookie || '__user';

    // Session expiration
    const EXPIRE = options.expire || '20 minutes';

    const PREFIX = options.prefix || '';

    const SUCCESS_LOGIN = options.success_login || '/';

    const USERS = NOSQL('users');

    NEWSCHEMA('Login').make(function (schema) {
        schema.define('name', 'String');
        schema.define('password', 'String');
        schema.define('roles', '[String]');

        schema.setSave(function ($) {
            let model = $.model.$clean();

            model.password = F.hash('sha512', model.password, options.salt);

            USERS.insert(model)
                .promise()
                .then(function () {
                    $.success();
                })
                .catch(function () {
                    $.invalid('error-login-create');
                })
            ;
        });

        schema.setUpdate(function ($) {
            let model = $.model.$clean();

            model.password = F.hash('sha512', model.password, options.salt);

            USERS.update(model)
                .where('id', model.id)
                .promise()
                .then(function () {
                    $.success();
                })
                .catch(function () {
                    $.invalid('error-login-create');
                })
            ;
        });

        schema.addWorkflow('login', function ($) {
            let model = $.model.$clean();

            USERS.find()
                .where('name', model.name)
                .where('password', F.hash('sha512', model.password, options.salt))
                .first()
                .fields('id')
                .promise()
                .then(function (user) {
                    if (user != null) {
                        user.ip = $.ip;
                        user.ticks = F.datetime.getTime();

                        $.controller.cookie(COOKIE, F.encrypt(user, options.secret), '7 days');
                        $.success();
                    } else {
                        $.invalid('error-users-credentials', F.translate('Invalid user credentials'));
                    }
                })
                .catch(function () {
                    $.invalid('error-users-credentials', F.translate('Invalid user credentials'));
                })
            ;
        });
    });

    NEWSCHEMA('User').make(function (schema) {
        schema.define('id', 'UID')(() => UID());
        schema.define('name', 'String');

        schema.addWorkflow('logout', function ($) {
            $.controller.cookie(COOKIE, '', '-1 day');
            $.controller.redirect(`${PREFIX}/login/`);
        });
    });

    // Total.js authorize delegate is executed on each request with except for a request to a static file
    F.onAuthorize = function (req, res, flags, callback) {
        let cookie = req.cookie(COOKIE);

        // Check the cookie length
        if (!cookie || cookie.length < 20) {
            return callback(false);
        }

        // Decrypt the cookie value
        cookie = F.decrypt(cookie, options.secret);
        if (!cookie) {
            return callback(false);
        }

        // Look into the session object whether the user is logged in
        let user = ONLINE[cookie.id];
        if (user) {
            // User is online, so we increase his expiration of session
            user.ticks = F.datetime;
            return callback(true, user);
        }

        // Session doesn't exist here, so we try to sign-in user because we have his ID
        USERS.find()
            .where('id', cookie.id)
            .first()
            .fields('id', 'name')
            .promise()
            .then(function (user) {
                if (user.roles) {
                    flags.push(...user.roles.map(i => '@' + i));
                }

                // We have the user so we can set the current timestamp (for his expiration)
                user.ticks = F.datetime;

                // Create a session
                ONLINE[user.id] = user;

                // Authorize the user
                callback(true, user);
            })
            .catch(function () {
                // If not, then remove the cookie
                res.cookie(COOKIE, '', '-1 day');
                return callback(false);
            })
        ;
    };

    // A simple service cleaner for expired sessions
    F.on('service', function (counter) {

        // Clean session each 5 minutes
        if (counter % 5 !== 0)
            return;

        let sessions = Object.keys(ONLINE);

        //  Set 'ticks' to -20 minutes from now
        let ticks = F.datetime.add('-' + EXPIRE);

        for (let i = 0, length = sessions.length; i < length; i++) {
            let session = ONLINE[sessions[i]];

            // Sessions will be removed when are older than "-20 minutes"
            if (session.ticks < ticks)
                delete ONLINE[sessions[i]];
        }
    });

    function view_login() {
        this.view('@auth_name/login', {success_url: SUCCESS_LOGIN});
    }

    F.route(`GET  ${PREFIX}/login/`, view_login, ['unauthorize']);
    F.route(`POST ${PREFIX}/login/`, ['*Login --> @login', 'unauthorize']);
    F.route(`GET  ${PREFIX}/logoff/`, ['*User --> @logout', 'authorize']);

    USERS.count().promise().then(function (count) {
        if (count === 0) {
            $MAKE('Login', {name: 'admin', password: 'admin', roles: ['admin']}, function (err, model) {
                model.$save();
            });
        }
    });

};