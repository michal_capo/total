exports.dependencies = ['v4_utils'];
exports.install = function (options) {

    U.check(options, Object);
    U.check(options.secret, String);
    U.check(options.salt, String);
    U.check(options.host, String);
    U.check(options.from, 'Email');

    // This object will contain all online users (their sessions)
    const ONLINE = {};

    // Cookie name
    const COOKIE = options.cookie || '__user';

    // Session expiration
    const EXPIRE = options.expire_session || '20 minutes';

    // Expire reset tokens
    const EXPIRE_RESET = options.expire_reset || '1 day';

    const PREFIX = options.prefix || '/auth';
    const SUCCESS_LOGIN = options.success_login || '/';

    const USERS = NOSQL('users');
    const RESET = NOSQL('users_reset');

    const LoginReset = {
        create(user_id) {
            let model = {
                user_id,
                hash: UID('reset_link'),
                ticks: F.datetime
            };

            return new Promise(async (resolve, reject) => {
                await RESET.remove().where('user_id', user_id).promise();
                await RESET.insert(model).promise();
                await RESET.find().first().where('user_id', user_id).promise()
                    .then(obj => resolve(obj))
                    .catch(err => reject(err))
            });
        },

        get_user(hash) {
            return RESET.find().first().where('hash', hash).promise();
        },

        equal(user_id, hash) {
            return RESET.find().where('user_id', user_id).where('hash', hash).promise();
        },

        remove(user_id) {
            return RESET.remove().where('user_id', user_id).promise();
        },

        all() {
            return RESET.find().promise();
        }
    };

    NEWSCHEMA('Login').make(function (schema) {
        schema.define('id', 'UID', true)(() => UID());
        schema.define('email', 'Email', true);
        schema.define('password', 'String', true);
        schema.define('roles', '[String]');

        schema.setSave(function ($) {
            let model = $.model.$clean();

            model.password = F.hash('sha512', model.password, options.salt);

            USERS.insert(model)
                .promise()
                .then(function () {
                    $.success();
                })
                .catch(function () {
                    $.invalid('error-login-create');
                })
            ;
        });

        schema.setUpdate(function ($) {
            let model = $.model.$clean();

            model.password = F.hash('sha512', model.password, options.salt);

            USERS.update(model)
                .where('id', model.id)
                .promise()
                .then(function () {
                    $.success();
                })
                .catch(function () {
                    $.invalid('error-login-create');
                })
            ;
        });

        schema.addWorkflow('login', function ($) {
            let model = $.model.$clean();

            USERS.find()
                .where('email', model.email)
                .where('password', F.hash('sha512', model.password, options.salt))
                .first()
                .fields('id')
                .promise()
                .then(function (user) {
                    if (user != null) {
                        user.ip = $.ip;
                        user.ticks = F.datetime.getTime();

                        $.controller.cookie(COOKIE, F.encrypt(user, options.secret), '7 days');
                        $.success();
                    } else {
                        $.invalid('error-users-credentials', F.translate('Invalid user credentials'));
                    }
                })
                .catch(function () {
                    $.invalid('error-users-credentials', F.translate('Invalid user credentials'));
                })
            ;
        });
    });

    NEWSCHEMA('User').make(function (schema) {
        schema.define('id', 'UID', true)(() => UID());
        schema.define('email', 'Email', true);

        schema.addWorkflow('logout', function ($) {
            $.controller.cookie(COOKIE, '', '-1 day');
            $.controller.redirect(`${PREFIX}/login/`);
        });
    });

    NEWSCHEMA('LoginReset').make(function (schema) {
        schema.define('user_id', 'UID', true);
        schema.define('password', 'String', true);
        schema.define('password2', 'String', true);

        schema.addWorkflow('change', function ($) {
            let model = $.model.$clean();

            if (model.password !== model.password2) {
                $.invalid('error-reset-mismatch', F.translate('Passwords doesn`t match'));
            } else {
                USERS.modify({password: F.hash('sha512', model.password, options.salt)})
                    .where('id', model.user_id)
                    .promise()
                    .then(function () {
                        $.success();
                        LoginReset.remove(model.user_id);
                    })
                    .catch(function () {
                        $.invalid('error-reset-create');
                    })
                ;
            }
        });
    });

    // Total.js authorize delegate is executed on each request with except for a request to a static file
    F.onAuthorize = function (req, res, flags, callback) {
        let cookie = req.cookie(COOKIE);

        // Check the cookie length
        if (!cookie || cookie.length < 20) {
            return callback(false);
        }

        // Decrypt the cookie value
        cookie = F.decrypt(cookie, options.secret);
        if (!cookie) {
            return callback(false);
        }

        // Look into the session object whether the user is logged in
        let user = ONLINE[cookie.id];
        if (user) {
            // User is online, so we increase his expiration of session
            user.ticks = F.datetime;
            flags.push(...user.roles.map(i => '@' + i));

            return callback(true, user);
        }

        // Session doesn't exist here, so we try to sign-in user because we have his ID
        USERS.find()
            .where('id', cookie.id)
            .first()
            .fields('id', 'email', 'roles')
            .promise()
            .then(function (user) {
                if (user.roles) {
                    flags.push(...user.roles.map(i => '@' + i));
                }

                // We have the user so we can set the current timestamp (for his expiration)
                user.ticks = F.datetime;
                user.has_role = (role) => role && user.roles && user.roles.indexOf(role) !== -1;

                // Create a session
                ONLINE[user.id] = user;

                // Authorize the user
                callback(true, user);
            })
            .catch(function () {
                // If not, then remove the cookie
                res.cookie(COOKIE, '', '-1 day');
                return callback(false);
            })
        ;
    };

    // Remove expired user sessions and reset tokens.
    U.crontab('*/5 * * * *', function () {
        let sessions = Object.keys(ONLINE);
        let ticks = F.datetime.add('-' + EXPIRE);
        let ticks_reset = F.datetime.add('-' + EXPIRE_RESET);

        for (let i = 0, length = sessions.length; i < length; i++) {
            let session = ONLINE[sessions[i]];

            // Sessions will be removed when expired
            if (session.ticks < ticks)
                delete ONLINE[sessions[i]];
        }

        LoginReset.all().then(function (tokens) {
            tokens.forEach(function (token) {
                if (token.ticks < ticks_reset)
                    LoginReset.remove(token.user_id);
            });
        });
    });

    function view_login() {
        let self = this;

        if (self.req.isAuthorized) {
            self.redirect('/');
        } else {
            this.view('@auth_email/login', {success_url: SUCCESS_LOGIN, prefix: PREFIX});
        }
    }

    async function view_reset(hash) {
        this.repository.prefix = PREFIX;
        this.repository.login_url = `${PREFIX}/login/`;

        const token = await LoginReset.get_user(hash);

        if (token == null) {
            this.view('@auth_email/reset_expired');
        } else {
            this.view('@auth_email/reset', {
                user_id: token.user_id
            });
        }
    }

    F.route(`GET  ${PREFIX}/login/`, view_login);
    F.route(`POST ${PREFIX}/login/`, ['*Login --> @login', 'unauthorize']);
    F.route(`GET  ${PREFIX}/logoff/`, ['*User --> @logout', 'authorize']);
    F.route(`GET  ${PREFIX}/reset/send/{id}/`, function (id) {
        const self = this;

        USERS.find().first().where('id', id).promise().then(function (user) {
            if (user) {
                LoginReset.create(id).then(function (model) {
                    if (model) {
                        const subject = `${options.host} - ${F.translate('password reset')}`;
                        const body = `${F.translate('To change your password please, copy/paste link below into your browser and follow instructions on web page.')}\r\n\r\n${self.host()}${PREFIX}/reset/${model.hash}/`;

                        Mail
                            .create(subject, body)
                            .from(options.from)
                            .to(user.email)
                            .send2()
                        ;

                        self.json('ok');
                    }
                });
            }
        })
    }, ['@admin']);
    F.route(`GET  ${PREFIX}/reset/{hash}/`, view_reset);
    F.route(`POST ${PREFIX}/reset/`, ['*LoginReset --> @change']);

    USERS.count().promise().then(function (count) {
        if (count === 0) {
            $MAKE('Login', {email: 'admin@admin.sk', password: 'admin', roles: ['admin']}, function (err, model) {
                model.$save();
            });
        }
    });

};