exports.id = 'v4_mailer';
exports.dependencies = ['v4_utils'];
exports.install = function () {
    const imap = require('imap');
    const simpleParser = require('mailparser').simpleParser;

    F.Mailer = {
        /*
            parseBody: function (body) {
                const parts = [];
                const attachments = [];

                let multi = false;

                if (body.split('\r\n')[0].indexOf('multi-part') !== -1) {
                    multi = true;
                }

                if (multi) {
                    let re = /(Content-.*):/;

                    let prev = 0;
                    let last = 0;
                    while (prev !== -1) {
                        let head = {};
                        let content = '';

                        prev = body.indexOf('Content-Type:', last);
                        last = body.indexOf('--------------', prev);

                        let end = false;
                        let temp = body.substring(prev, last);
                        let lines = temp.split('\r\n');

                        let head_name = null;
                        let head_value = '';
                        lines.forEach(function (l) {
                            if (!end) {
                                switch (l[0]) {
                                    case 'C':
                                        if (head_name != null) {
                                            head[head_name] = head_value;

                                            head_name = null;
                                            head_value = '';
                                        }

                                        head_name = l.match(re)[1];
                                        head_value += l.replace(re, '').trim();
                                        break;
                                    case ' ':
                                        if (l.trim() === '') {
                                            end = true;
                                        } else {
                                            head_value += l.trim();
                                        }
                                        break;
                                    case undefined:
                                        end = true;
                                        break;
                                }

                                if (end && head_name != null) {
                                    head[head_name] = head_value;
                                    head_name = null;
                                    head_value = '';
                                }
                            } else {
                                content += l + EOL;
                            }
                        });

                        if (content.trim() !== '') {
                            let t = head['Content-Type'];
                            let d = head['Content-Disposition'];
                            let e = head['Content-Transfer-Encoding'];

                            if (d) {
                                t = t.split(';');
                                d = d.split(';');
                                attachments.push({
                                    type: t[0],
                                    encoding: e,
                                    name: d[1].split('=')[1].replace(/"/g, ''),
                                    content: t[0].startsWith('text') ? content : content.replace(new RegExp(EOL, 'g'), '')
                                });
                            } else {
                                t = t.split(';');
                                parts.push({type: t[0], endocidng: e, content});
                            }
                        }
                    }
                } else {
                    parts.push({encoding: 'utf-8', type: 'text/plain', content: body.trim()});
                }

                return {parts, attachments};
            },
        */

        connect: function (user, password, options) {
            const EOL = require('os').EOL;

            if (options == null) {
                U.check(F.config['imap-options'], Object, 'imap-options not found in config file');
                options = F.config['imap-options'];
            }

            options.user = user;
            options.password = password;

            U.check(options, Object);
            U.check(options.user, String);
            U.check(options.password, String);
            U.check(options.host, String);
            U.check(options.port, Number);

            const client = new imap(options);
            const lis_end = [];
            const lis_error = [];
            const lis_message = [];

            const send_error = err => err && lis_error.forEach(fn => fn(err));
            const send_message = msg => lis_message.forEach(fn => fn(msg));

            function fetch_emails(ids) {
                const f = client.fetch(ids, {markSeen: true, bodies: ['']});

                f.once('error', send_error);
                f.on('message', function (msg, id) {
                    msg.on('body', function (stream) {
                        let buffer = '';
                        stream.on('data', function (chunk) {
                            buffer += chunk.toString('utf8');
                        });
                        stream.once('end', function () {
                            simpleParser(buffer)
                                .then(send_message)
                                .catch(send_error)
                            ;
                        });
                    });
                });
                f.on('end', function () {
                    client.move(ids, 'Archives', send_error);
                })
            }

            function check_emails() {
                client.search(['ALL'], function (err, result) {
                    if (err)
                        send_error(err);
                    else if (result && result.length > 0)
                        fetch_emails(result);
                });
            }

            const temp = {
                watch(box_name = 'INBOX') {
                    client.once('ready', function () {
                        client.addBox('Archives');
                        client.openBox(box_name, false, send_error);
                    });

                    client.on('mail', check_emails);
                    // client.on('update', check_emails);
                    client.once('end', () => lis_end.forEach(fn => fn()));
                    client.once('error', send_error);
                    client.connect();

                    return temp;
                },

                stop() {
                    client.end();

                    return temp;
                },

                /**
                 * Handle incoming events with these patterns: 'message, (msg) =>', 'error, (err) =>', 'end, () =>'
                 *
                 * @param event can be 'message', 'error', 'end'
                 * @param fn call on given event
                 */
                on(event, fn) {
                    switch (event) {
                        case 'end':
                            lis_end.push(fn);
                            break;
                        case 'error':
                            lis_error.push(fn);
                            break;
                        case 'message':
                            lis_message.push(fn);
                            break;
                    }

                    return temp;
                }
            };

            return temp;
        }
    };

};

exports.uninstall = function () {
    delete F.Mailer;
};
