exports.id = 'v4_arrays';
exports.install = function () {
    const check_undefined = (obj) => {
        if (obj != null) throw new Error('is defined');
    };

    check_undefined(Array.prototype.equals);
    Array.prototype.equals = function (array) {
        if (!array)
            return false;

        this.sort();
        array.sort();

        if (this.length !== array.length)
            return false;

        for (let i = 0, l = this.length; i < l; i++) {
            if (this[i] instanceof Array && array[i] instanceof Array) {
                if (!this[i].equals(array[i]))
                    return false;
            }
            else if (this[i] !== array[i]) {
                return false;
            }
        }
        return true;
    };

    check_undefined(Array.prototype.flatten);
    Array.prototype.flatten = function () {
        return [].concat.apply([], this);
    };
};
