exports.id = 'v4_utils';
exports.install = function () {
    U.check = function (obj, type, msg) {
        switch (type) {
            case 'undefined':
                if (obj != null) {
                    throw new Error(msg || obj + ': is defined.');
                }
                break;

            case 'defined':
                if (obj == null) {
                    throw new Error(msg || obj + ': is not defined.');
                }
                break;

            case Number:
            case 'number':
                if (typeof obj !== 'number') {
                    throw new Error(msg || obj + ': is not number.');
                }
                break;

            case String:
            case 'string':
                if (typeof obj !== 'string') {
                    throw new Error(msg || obj + ': is not string.');
                }
                break;

            case Object:
            case 'object':
                if (!(obj instanceof Object)) {
                    throw new Error(msg || obj + ': is not object.');
                }
                break;

            case Date:
            case 'date':
                if (!(obj instanceof Date)) {
                    throw new Error(msg || obj + ': is not a date.');
                }
                break;
            case 'email':
            case 'Email':
                if (!(obj && obj.isEmail())) {
                    throw new Error(msg || obj + ': is not an email.');
                }
                break;

            case Array:
            case 'array':
                if (!(obj && U.isArray(obj))) {
                    throw new Error(msg || obj + ': is not an array.');
                }
                break;

            case Function:
            case 'function':
                if (!(obj && U.isFunction(obj))) {
                    throw new Error(msg || obj + ': is not an function.');
                }
                break;

            default:
                throw new Error('uknown type');
        }
    };

    U.check(U.isFunction, 'undefined');
    U.isFunction = function (obj) {
        return obj && {}.toString.call(obj) === '[object Function]';
    };

    U.check(U.isString, 'undefined');
    U.isString = function (obj) {
        return typeof obj === 'string';
    };

    U.check(U.stringify, 'undefined');
    U.stringify = function (o) {
        return JSON.stringify(o, null, 4);
    };

    /**
     * Round number to specific digits.
     */
    U.check(U.round, 'undefined');
    U.round = function (number, precision = 2) {
        return Math.round(number * Math.pow(10, precision)) / Math.pow(10, precision);
    };

    U.check(U.liner, 'undefined');
    U.liner = function (match, fn) {
        const {Writable} = require('stream');
        let temp = '';

        return new Writable({
            write(chunk, encoding, cb) {
                let decoded = chunk;

                if (encoding === 'buffer') {
                    decoded = chunk.toString();
                }

                let include_last = decoded.endsWith(match);
                let lines = decoded.split(match);

                if (lines.length > 0) {
                    lines.splice(0, 1, temp + lines[0]);
                    temp = '';

                    if (!include_last) {
                        temp = lines[lines.length - 1];
                        lines.splice(lines.length - 1, 1);
                    }

                    lines.forEach(line => fn(line));
                }

                cb();
            }
        });
    };

    /**
     * Create promise from function with a callback. First create synchron version of asynchrone function:
     * <pre>const get = U.promise(http.get, 'stream')</pre>
     * then you can call it like this:
     * <prev>let xml = get('some url')</prev>
     */
    U.check(U.promise, 'undefined');
    U.promise = function (fn, response = 'cb') {
        switch (response) {
            case 'stream':
                response = function (resolve, reject) {
                    return function (res) {
                        let content = '';
                        res.setEncoding("utf8");

                        res.on("data", data => content += data);
                        res.on("end", () => resolve(content));
                        res.on('error', e => reject(e))
                    }
                };
                break;
            case 'cb':
                response = function (resolve, reject) {
                    return function (err, result) {
                        if (err) {
                            reject(err);
                        } else {
                            resolve(result)
                        }
                    }
                };
                break;
        }

        if (response == null) throw new Error('Response is not defined.');

        return function (...args) {
            return new Promise((resolve, reject) => {
                fn(...args, response(resolve, reject))
            });
        }
    };

    U.check(U.lowercase, 'undefined');
    U.lowercase = function (val) {
        return val == null ? val : String(val).toLowerCase();
    };

    U.check(U.uppercase, 'undefined');
    U.uppercase = function (val) {
        return val == null ? val : String(val).toUpperCase();
    };

    U.check(U.crontab_match, 'undefined');
    U.crontab_match = function (stamp, cron) {
        U.check(stamp, Number);
        U.check(cron, String);

        const reg = /(\d+)(?:-(\d+))?(?:\/(\d+))?/;

        if (isNaN(stamp)) throw new Error('Stamp cannot be a NaN.');

        let at = cron.split(' ');
        if (at.length !== 5) throw new Error('Cron doesn`t match "* * * * *" pattern.');

        let d = new Date(stamp);
        let day = d.getDate(), month = d.getMonth() + 1, hours = d.getHours(), minutes = d.getMinutes(),
            day_week = d.getDay();

        function from_to(from, to, max) {
            to = to || max;
            let result = [];

            for (let i = from; i <= to; i += 1) {
                result.push(i);
            }

            return result;
        }

        function parse(at, min, max) {
            let result = [];

            at.split(',').forEach(function (p) {
                if (reg.test(p)) {
                    let r = p.match(reg);
                    if (r[2] == null && r[3] == null) {
                        result.push(+r[1]);
                    } else {
                        let temp = from_to(+r[1], +r[2], max);
                        if (r[3] != null) {
                            for (let i = min; i < temp.length; i += +r[3]) {
                                result.push(temp[i]);
                            }
                        } else {
                            result.push(temp);
                        }
                    }
                }
            });

            return [].concat.apply([], result);
        }

        let _1 = at[0] === '*' ? true : parse(at[0], 0, 59).indexOf(minutes) !== -1;
        let _2 = at[1] === '*' ? true : parse(at[1], 0, 23).indexOf(hours) !== -1;
        let _3 = at[2] === '*' ? true : parse(at[2], 1, 31).indexOf(day) !== -1;
        let _4 = at[3] === '*' ? true : parse(at[3], 1, 12).indexOf((month)) !== -1;
        let _5 = at[4] === '*' ? true : parse(at[4], 0, 6).indexOf(day_week) !== -1;

        return _1 && _2 && _3 && _4 && _5;
    };

    U.check(U.crontab, 'undefined');
    U.crontab = function (cron, fn) {
        if (!F.global.CRONTAB) {
            F.global.CRONTAB = {
                crons: [],

                init: function () {
                    const self = this;

                    function exec() {
                        const now = new Date().getTime();

                        self.crons.forEach(function ({cron, fn}) {
                            if (U.crontab_match(now, cron)) {
                                fn();
                            }
                        })
                    }

                    exec();

                    setInterval(exec, 60 * 1000);
                },

                add(cron, fn) {
                    this.crons.push({cron, fn});
                }
            };

            F.global.CRONTAB.init();
        }

        F.global.CRONTAB.add(cron, fn);
    };

    U.check(U.parse_xml2, 'undefined');
    U.parse_xml2 = function (xml) {
        const regex = /<(\w+).*?>/g;
        const result = {};
        let temp;

        while ((temp = regex.exec(xml.replace(/\n/g, ''))) != null) {
            let name = temp[1];
            let from = regex.lastIndex;
            let length = temp.input.substr(regex.lastIndex).indexOf(`</${temp[1]}>`);
            // let to = regex.lastIndex + length;
            let value = temp.input.substr(from, length).trim();

            if (value != null && value.trim().length > 0)
                if (result[name] == null)
                    result[name] = value;
                else if (Array.isArray(result[name]))
                    result[name].push(value);
                else
                    result[name] = [result[name], value];
        }

        return result;
    };
};
