exports.id = 'v4_errors';
exports.install = function () {
    ERRORS = function (hour_to_mute = 24, db_name = 'errors') {
        const errors = NOSQL(db_name);

        return {
            error_report(key, sub_key = '', action) {
                if (typeof sub_key === 'function') {
                    action = sub_key;
                    sub_key = '';
                }

                let value = errors.get(key + sub_key);

                if (value == null || Date.compare(value.add('h', hour_to_mute), new Date()) === -1) {
                    action();
                    errors.meta(key + sub_key, new Date());
                }
            },

            error_clear(key, sub_key = '') {
                errors.set(key + sub_key, null);
            }
        }
    };
};
exports.uninstall = function () {
    delete ERRORS;
};
