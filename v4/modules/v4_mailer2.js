exports.id = 'v4_mailer';
exports.dependencies = ['v4_utils'];
exports.install = function () {
    F.Mailer = function (options) {
        const EOL = require('os').EOL;

        if (options == null) {
            U.check(F.config['imap-options'], Object, 'imap-options not found in config file');
            options = F.config['imap-options'];
        }

        U.check(options, Object);
        U.check(options.user, String);
        U.check(options.password, String);
        U.check(options.host, String);
        U.check(options.port, Number);

        const imap = require('imap');
        const client = new imap(options);
        const lis_end = [];
        const lis_error = [];
        const lis_message = [];

        function read_emails() {
            function parseBody(body) {
                const parts = [];
                const attachments = [];

                let multi = false;

                if (body.split('\r\n')[0].indexOf('multi-part') !== -1) {
                    multi = true;
                }

                if (multi) {
                    let re = /(Content-.*):/;

                    let prev = 0;
                    let last = 0;
                    while (prev !== -1) {
                        let head = {};
                        let content = '';

                        prev = body.indexOf('Content-Type:', last);
                        last = body.indexOf('--------------', prev);

                        let end = false;
                        let temp = body.substring(prev, last);
                        let lines = temp.split('\r\n');

                        let head_name = null;
                        let head_value = '';
                        lines.forEach(function (l) {
                            if (!end) {
                                switch (l[0]) {
                                    case 'C':
                                        if (head_name != null) {
                                            head[head_name] = head_value;

                                            head_name = null;
                                            head_value = '';
                                        }

                                        head_name = l.match(re)[1];
                                        head_value += l.replace(re, '').trim();
                                        break;
                                    case ' ':
                                        if (l.trim() === '') {
                                            end = true;
                                        } else {
                                            head_value += l.trim();
                                        }
                                        break;
                                    case undefined:
                                        end = true;
                                        break;
                                }

                                if (end && head_name != null) {
                                    head[head_name] = head_value;
                                    head_name = null;
                                    head_value = '';
                                }
                            } else {
                                content += l + EOL;
                            }
                        });

                        if (content.trim() !== '') {
                            let t = head['Content-Type'];
                            let d = head['Content-Disposition'];
                            let e = head['Content-Transfer-Encoding'];

                            if (d) {
                                t = t.split(';');
                                d = d.split(';');
                                attachments.push({
                                    type: t[0],
                                    encoding: e,
                                    name: d[1].split('=')[1].replace(/"/g, ''),
                                    content: t[0].startsWith('text') ? content : content.replace(new RegExp(EOL, 'g'), '')
                                });
                            } else {
                                t = t.split(';');
                                parts.push({type: t[0], endocidng: e, content});
                            }
                        }
                    }
                } else {
                    parts.push({encoding: 'utf-8', type: 'text/plain', content: body.trim()});
                }

                return {parts, attachments};
            }

            client.search(['UNSEEN'], function (err, result) {
                if (err) throw err;
                if (result && result.length > 0) {
                    let f = client.fetch(result, {
                        markSeen: true,
                        bodies: ['HEADER.FIELDS (FROM SUBJECT)', 'TEXT']
                    });
                    f.on('message', function (msg, seqno) {
                        let mail = {seqno};

                        msg.on('body', function (stream) {
                            let buffer = '';
                            stream.on('data', function (chunk) {
                                buffer += chunk.toString('utf8');
                            });
                            stream.once('end', function () {
                                if (mail.header == null) {
                                    mail.header = imap.parseHeader(buffer);
                                } else {
                                    let {parts, attachments} = parseBody(buffer);
                                    mail.body = parts;
                                    mail.attachments = attachments;
                                }
                            });
                        });
                        /*
                            msg.once('attributes', function (attrs) {
                                mail.attributes = attrs;
                            });
                        */
                        msg.once('end', function () {
                            lis_message.forEach(fn => fn(mail));
                        });
                    });
                    f.once('error', function (err) {
                        lis_error.forEach(fn => fn(err))
                    });
                    /*
                        f.once('end', function () {
                            client.end();
                        });
                    */
                }
            });
        }

        const temp = {
            watch_box(box_name) {
                client.once('ready', function () {
                    client.openBox(box_name, false, function (err) {
                        if (err) {
                            lis_error.forEach(fn => fn(err))
                        }
                    });
                });

                client.on('mail', () => read_emails());
                client.once('end', () => lis_end.forEach(fn => fn()));
                client.once('error', (err) => lis_error.forEach(fn => fn(err)));
                client.connect();

                return temp;
            },

            stop() {
                client.end();

                return temp;
            },

            /**
             * Handle incoming events with these patterns: 'message, (msg) =>', 'error, (err) =>', 'end, () =>'
             *
             * @param event can be 'message', 'error', 'end'
             * @param fn call on given event
             */
            on(event, fn) {
                switch (event) {
                    case 'end':
                        lis_end.push(fn);
                        break;
                    case 'error':
                        lis_error.push(fn);
                        break;
                    case 'message':
                        lis_message.push(fn);
                        break;
                }

                return temp;
            }
        };

        return temp;
    };

};

exports.uninstall = function () {
    delete F.Mailer;
};
