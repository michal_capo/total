exports.id = 'v4_dates';
exports.install = function () {
    const check_undefined = (obj) => {
        if (obj != null) throw new Error('is defined');
    };

    check_undefined(Date.prototype.explode);
    Date.prototype.explode = function (to_utc = false) {
        let temp = {
            day: this.getDate(),
            day_week: this.getDay(),
            month: this.getMonth() + 1,
            year: this.getFullYear(),
            hours: this.getHours(),
            minutes: this.getMinutes(),
            seconds: this.getSeconds(),
            milis: this.getMilliseconds(),
        };

        if (to_utc) {
            temp = {
                day: this.getUTCDate(),
                day_week: this.getUTCDay(),
                month: this.getUTCMonth() + 1,
                year: this.getUTCFullYear(),
                hours: this.getUTCHours(),
                minutes: this.getUTCMinutes(),
                seconds: this.getUTCSeconds(),
                milis: this.getUTCMilliseconds()
            }
        }

        return temp;
    };

    /**
     * Set date to start of month or year.
     *
     * @param type, can be month or year
     */
    check_undefined(Date.prototype.startOf);
    Date.prototype.startOf = function (type = 'month') {
        const {day, month} = this.explode();

        let dt = new Date(this.getTime());

        switch (type) {
            case 'month':
                dt = dt.add('d', -1 * (day - 1)).extend('00:00:00.000');
                break;
            case 'year':
                dt = dt.add('d', -1 * (day - 1)).add('M', -1 * (month - 1)).extend('00:00:00.000');
                break;
        }

        return dt;
    };

    /**
     * Set date to end of month or year.
     *
     * @param type, can be month or year
     */
    check_undefined(Date.prototype.endOf);
    Date.prototype.endOf = function (type = 'month') {
        const {day, month, year} = this.explode();
        const leap = (year % 100 === 0) ? (year % 400 === 0) : (year % 4 === 0);
        const days_count = [31, 28 + (leap ? 1 : 0), 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];

        let dt = new Date(this.getTime());

        switch (type) {
            case 'month':
                dt = dt.add('d', days_count[month - 1] - day).extend('23:59:59.999');
                break;
            case 'year':
                dt = dt.add('M', 12 - month).add('d', 31 - day).extend('23:59:59.999');
                break;
        }

        return dt;
    };

    Date.prototype.setWeek = function (week) {
        const year = this.getFullYear();
        const january_1 = new Date(year, 0, 1);

        return january_1.add('d', 1 - january_1.getDay()).add('d', (week - 1) * 7).extend('00:00:00.000');
    };

    check_undefined(Date.prototype.set);
    Date.prototype.set = function (type, count) {
        let dt = new Date(this.getTime());

        switch (type) {
            case 's':
            case 'ss':
            case 'sec':
            case 'second':
            case 'seconds':
                dt = new Date(dt.setSeconds(count));
                break;
            case 'm':
            case 'mm':
            case 'min':
            case 'minute':
            case 'minutes':
                dt = new Date(dt.setMinutes(count));
                break;
            case 'h':
            case 'hh':
            case 'H':
            case 'HH':
            case 'hour':
            case 'hours':
                dt = new Date(dt.setHours(count));
                break;
            case 'M':
            case 'MM':
            case 'MMM':
            case 'MMMM':
            case 'month':
                dt = new Date(dt.setMonth(count - 1));
                break;
            case 'd':
            case 'dd':
            case 'ddd':
            case 'dddd':
            case 'day':
                dt = new Date(dt.setDate(count));
                break;
            case 'y':
            case 'yy':
            case 'yyy':
            case 'yyyy':
            case 'year':
                dt = new Date(dt.setFullYear(count));
                break;
        }

        return dt;
    };

    check_undefined(Date.range_week);
    Date.range_week = function (year, week) {
        const dt = new Date().set('y', year).setWeek(week);

        return {from: dt, to: dt.add('d', 6).extend('23:59:59.999')};
    };

    check_undefined(Date.range_month);
    Date.range_month = function (year, month) {
        const dt = new Date().set('y', year).set('M', month);

        return {from: dt.startOf(), to: dt.endOf()};
    }
};
