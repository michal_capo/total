exports.id = 'v4_http';
exports.install = function () {
    HTTP = function HTTP(url, options, body, encoding = 'utf8') {
        let parsed = url.split(/(\w+\s)?(.*)/);

        switch (parsed[1] == null ? null : parsed[1].trim().toUpperCase()) {
            case 'POST':
                return HTTP.post(parsed[2], options, body, encoding);

            case 'PUT':
                return HTTP.put(parsed[2], options, body, encoding);

            case 'DELETE':
                return HTTP.delete(parsed[2], options, body, encoding);

            case 'HEAD':
                return HTTP.head(parsed[2], options, body, encoding);

            default:
                return HTTP.get(parsed[2], options, body, encoding)
        }
    };

    HTTP.create_method = function (method = 'GET') {
        const Url = require('url');
        const Http = require('http');
        const Https = require('https');
        const handle_response = function (ok, fail, encoding = 'utf8') {
            return function (res) {
                let body = '';

                res.setEncoding(encoding);
                res.on('error', e => fail(e));
                res.on("data", data => body += data);
                res.on("end", () => ok({
                    headers: res.headers,
                    httpVersion: res.httpVersion,
                    method: res.method,
                    statusCode: res.statusCode,
                    statusMessage: res.statusMessage,
                    url: res.url,
                    body
                }));
            }
        };

        return function (url, options, body, encoding = 'utf8') {
            if (body instanceof Object)
                body = JSON.stringify(body);

            return new Promise(function (resolve, reject) {
                let ok = response => response.statusCode >= 400 ? reject(response) : resolve(response);
                let fail = response => reject(response);
                let _req;
                let _url = Url.parse(url, true);
                let _response = handle_response(ok, fail, encoding);

                _url = Object.assign(_url, options);
                _url.method = method;

                switch (_url.protocol) {
                    case 'http:':
                        _req = Http.request(_url, _response);
                        break;
                    case 'https:':
                        _req = Https.request(_url, _response);
                        break;
                    default:
                        throw new Error('Not supported protocol: ' + _url.protocol);
                }

                if (body)
                    _req.write(body);

                _req.end();
            });
        }
    };

    HTTP.get = HTTP.create_method('GET');
    HTTP.post = HTTP.create_method('POST');
    HTTP.put = HTTP.create_method('PUT');
    HTTP.delete = HTTP.create_method('DELETE');
    HTTP.head = HTTP.create_method('HEAD');
};

exports.uninstall = function () {
    delete HTTP;
};
