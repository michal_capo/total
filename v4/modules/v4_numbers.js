exports.id = 'v4_numbers';
exports.install = function () {
    const check_undefined = (obj) => {
        if (obj != null) throw new Error('is defined');
    };

    check_undefined(Number.prototype.round_to);
    Number.prototype.round_to = function (precision = 1) {
        return Number((Math.round(this / precision) * precision).toPrecision(15));
    };

    check_undefined(Number.prototype.radd);
    Number.prototype.radd = function (val) {
        return Number((this + val).toPrecision(12));
    };

    check_undefined(Number.prototype.repeat);
    Number.prototype.repeat = function (val) {
        let result = '';

        for (let i = 0; i < this; i++) {
            result += val;
        }

        return result;
    };

    check_undefined(Number.prototype.vat);
    Number.prototype.vat = function (vat, precision = 2) {
        return (this / (100 + vat) * vat).round(precision);
    };

    check_undefined(Number.prototype.vat_rest);
    Number.prototype.vat_rest = function (vat, precision = 2) {
        return (this / (100 + vat) * 100).round(precision);
    };
};
