TEST('Date week range', function () {
    let range = Date.range_week(2018, 1);
    OK(range.from.getDate() === 1);
    OK(range.from.getMonth() === 0);
    OK(range.from.getFullYear() === 2018);
    OK(range.to.getDate() === 7);
    OK(range.to.getMonth() === 0);
    OK(range.to.getFullYear() === 2018);

    range = Date.range_week(2018, 5);
    OK(range.from.getDate() === 29);
    OK(range.from.getMonth() === 0);
    OK(range.from.getFullYear() === 2018);
    OK(range.to.getDate() === 4);
    OK(range.to.getMonth() === 1);
    OK(range.to.getFullYear() === 2018);

    range = Date.range_week(2020, 1);
    OK(range.from.getDate() === 30);
    OK(range.from.getMonth() === 11);
    OK(range.from.getFullYear() === 2019);
    OK(range.to.getDate() === 5);
    OK(range.to.getMonth() === 0);
    OK(range.to.getFullYear() === 2020);

    range = Date.range_week(2025, 52);
    OK(range.from.getDate() === 22);
    OK(range.from.getMonth() === 11);
    OK(range.from.getFullYear() === 2025);
    OK(range.to.getDate() === 28);
    OK(range.to.getMonth() === 11);
    OK(range.to.getFullYear() === 2025);

    range = Date.range_week(2026, 1);
    OK(range.from.getDate() === 29);
    OK(range.from.getMonth() === 11);
    OK(range.from.getFullYear() === 2025);
    OK(range.to.getDate() === 4);
    OK(range.to.getMonth() === 0);
    OK(range.to.getFullYear() === 2026);
});

TEST('Date month range', function () {
    let range = Date.range_month(2018, 4);
    OK(range.from.getDate() === 1);
    OK(range.from.getMonth() === 3);
    OK(range.from.getFullYear() === 2018);
    OK(range.to.getDate() === 30);
    OK(range.to.getMonth() === 3);
    OK(range.to.getFullYear() === 2018);

    range = Date.range_month(2020, 2);
    OK(range.from.getDate() === 1);
    OK(range.from.getMonth() === 1);
    OK(range.from.getFullYear() === 2020);
    OK(range.to.getDate() === 29);
    OK(range.to.getMonth() === 1);
    OK(range.to.getFullYear() === 2020);

});
